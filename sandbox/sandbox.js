const lighthouse = require('lighthouse');
const chromeLauncher = require('chrome-launcher');

const launchChromeAndRunLighthouse = url => {
    return chromeLauncher.launch().then(chrome => {
        const opts = {
            port: chrome.port
        };
        lighthouse(url, opts).then(results => {
            chrome.kill().then(() => results.report);
        });
    });
};

launchChromeAndRunLighthouse('https://www.simplon.co').then(results => {
    console.log(results);
})