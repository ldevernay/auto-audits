const lighthouse = require('lighthouse');
const chromeLauncher = require('chrome-launcher');

// Pour lancer l'audit lighthouse : 
// * on crée une instance de Chrome
exports.launchAudits = url => chromeLauncher.launch().then(chrome => {
    const opts = {
        port: chrome.port
    };
    // * on lance l'audit lighthouse
    return lighthouse(url, opts).then(results => {
        // * on ferme l'instane de chrome avant d'envoyer les résultats
        return chrome.kill().then(() => results.report);
    });
});

// Comparaison de rapport
exports.compareReports = (from, to) => {

};