# auto-audits

Un outil pour faciliter les audits (écoconception, performance et accessibilité) afin de suivre des indicateurs tout au long d'un projet. 

## How-to
Lancer le projet :
```
npm install
node server.js
```

Se connecter au "localhost:3001", saisir une URL et laisser la magie agir!