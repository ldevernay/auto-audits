const express = require('express');
const bodyparser = require('body-parser');
const { launchAudits } = require('./utils/audits');
const url = require('url');
const fs = require('fs');

const REPORTS_DIRNAME = 'public/reports/';

let app = express();

app.use(bodyparser.urlencoded({ extended: false }));
app.use(express.static('public'));

app.set('view engine', 'ejs');

app.get('/', (req, res) => {
    res.render('select_site');
});

// Route qui présente les différentes possibilités pour un site donné
app.post('/menu_audit', (req, res) => {
    let url = req.body.url;
    const urlObj = new URL(url);
    const hostName = urlObj.host.replace('www.', "").replace(/\//g, "_");
    res.render('launch_audit', {url, hostName});
});

// Route qui permet de lancer l'audit lighthouse
app.post('/start_audit', (req, res) => {
    let url = req.body.url;
    console.log(`URL utilisée : ${url}`);
    // Création d'un dossier pour stocker les rapports d'audits
    const urlObj = new URL(url);
    const hostName = urlObj.host.replace('www.', "").replace(/\//g, "_");
    let dirName = hostName;
    if (urlObj.pathname !== "/") {
        // On remplace les "/" par des "_" pour ne pas avoir de souci avec le nom du dossier
        dirName = dirName + urlObj.pathname.replace(/\//g, "_");
    }
    dirName = REPORTS_DIRNAME + dirName;
    // Si le répertoire n'existe pas déjà, on le crée
    if (!fs.existsSync(dirName)) {
        fs.mkdirSync(dirName);
    }
    // Lancement des audits
    launchAudits(url).then(results => {
        // On ajoute la date au nom du rapport d'audit
        // Pour éviter des ennuis avec windows, on remplace ":" par "_"
        let resultsObj = JSON.parse(results);
        const fetchTime = resultsObj["fetchTime"].replace(/:/g, "_");
        // On crée le fichier
        fs.writeFile(`${dirName}/${fetchTime}.json`, results, err => {
            if (err) throw err;
        });
        // On récupère la liste des fichiers déjà créés dans ce répertoire
        let path = require('path');
        let dir = `${path.resolve('.')}/${dirName}`;
        console.log("Dir results : " + dir);
        fs.readdir(dir, (err, list) => {
            if (err) { throw err; }
            else {
                res.render('results_audit', { results, hostName });
            }
        })
    });
});

// La route qui présente la liste des rapports dispo pour un site
app.get('/download/:hostName', (req, res) => {
    const hostName = req.params.hostName;
    let dirName = REPORTS_DIRNAME +  hostName;

    // On récupère la liste des fichiers déjà créés dans ce répertoire
    let path = require('path');
    let dir = `${path.resolve('.')}/${dirName}`;
    console.log("Dir DL : " + dir);
    fs.readdir(dir, (err, list) => {
        if (err) { throw err; }
        else {
            res.render('download', { list, hostName });
        }
    });
});

// La route qui permet de télécharger un fichier
app.get('/download/:hostName/:file', (req, res) => {
    const hostName = req.params.hostName;
    const file = req.params.file;
    console.log("Hostname : " + hostName);
    console.log("File : " + file);

    let path = require('path');
    path = `${path.resolve(".")}/${REPORTS_DIRNAME}/${hostName}/${file}`;
    console.log("Path : " + path);
    res.download(path);
});

app.listen(3001, (req, res) => console.log("Listening to port 3001"));